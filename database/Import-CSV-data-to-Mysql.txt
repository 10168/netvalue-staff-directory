
/ ***** import CSV file to Mysql database *****/
1. Create a database and a table netvalue_staff with same number of fields matchs the CVS fields
2. Use phpMyAdmin to import CSV to this database table netvalue_staff
3. Add and adjust the fields of table netvalue_staff
4. Create a netvalue_departments table to store departments data

/ ***** Get departments data from netvalue_staff table and insert into netvalue_departments table *****/

INSERT INTO netvalue_departments (name)
    SELECT department
    FROM netvalue_staff
    GROUP BY department

/ ***** Set the department id of netvalue_staff table based on netvalue_departments table *****/

UPDATE netvalue_staff a
LEFT JOIN netvalue_departments b ON a.department = b.name
SET department_id = b.id


/ ***** Change the department_id as a Foreign key constraints between netvalue_staff and netvalue_departments table  *****/

ALTER TABLE `netvalue_staff` ADD  FOREIGN KEY (`department_id`) REFERENCES `netvalue_departments`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

