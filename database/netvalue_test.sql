
--
-- Database: `netvalue_test`
--
CREATE DATABASE IF NOT EXISTS `netvalue_test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `netvalue_test`;

-- --------------------------------------------------------

--
-- Table structure for table `netvalue_departments`
--

DROP TABLE IF EXISTS `netvalue_departments`;
CREATE TABLE IF NOT EXISTS `netvalue_departments` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='NetValue Departments Test Data';

--
-- Dumping data for table `netvalue_departments`
--

REPLACE INTO `netvalue_departments` (`id`, `name`) VALUES
(1, 'Accounts'),
(2, 'Engineering'),
(3, 'Human Resources'),
(4, 'IT Support'),
(5, 'Legal'),
(6, 'Marketing'),
(7, 'Research'),
(8, 'Sales');

-- --------------------------------------------------------

--
-- Table structure for table `netvalue_staff`
--

DROP TABLE IF EXISTS `netvalue_staff`;
CREATE TABLE IF NOT EXISTS `netvalue_staff` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `department_id` int(10) UNSIGNED DEFAULT NULL,
  `firstname` varchar(60) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `department_name` varchar(50) DEFAULT NULL,
  `profile` text NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `department_id` (`department_id`),
  KEY `firstname` (`firstname`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1 COMMENT='NetValue Staff Test Data';

--
-- Dumping data for table `netvalue_staff`
--

REPLACE INTO `netvalue_staff` (`id`, `user_id`, `department_id`, `firstname`, `lastname`, `department_name`, `profile`, `avatar`) VALUES
(1, 0, 8, 'Salina', 'Keithley', 'Sales', 'Lorem ipsum dolor sit amet, usu ut feugiat propriae vituperatoribus. No nec numquam appellantur, qui populo persius offendit eu, ne sed solum fugit. Te his feugiat omittantur, at eam noster voluptaria inciderint. At doming eirmod labitur usu, eam option vidisse diceret te. Etiam inimicus an vix, nostro maiestatis vix et. Vel ut quis error causae.', ''),
(2, 0, 8, 'Brianne', 'Farago', 'Sales', 'Nec ex augue labore, id possit omittam ullamcorper his. No vix ridens scaevola, ut laboramus argumentum vim. Ut singulis periculis scriptorem vix, volutpat ullamcorper sit te. Ex quod partem per. Eius deleniti at sit, regione phaedrum eu qui, at vocent pericula mea. Sale pertinacia ad vis, volumus omittam an mel, constituto mediocritatem sea ei.', ''),
(3, 0, 8, 'Denyse', 'Polich', 'Sales', 'Cum ei salutatus honestatis, enim minim eirmod mel ei. His nostro inermis persecuti eu. Est id cetero contentiones, ius et veri meis. Id ipsum definiebas nam, epicurei pertinax ad mea, vim reque placerat ut.', ''),
(4, 0, 8, 'Amada', 'Afanador', 'Sales', 'Fabellas euripidis pro no. Nam legere eloquentiam ei, mei quod fabulas pertinacia at. Duo no latine vidisse pericula, illud animal accusam mea no. Mazim sadipscing ad vis, pro et graeci intellegam. Id vim utinam docendi legendos, has in soleat suavitate. Mei ne unum omnium aliquam.', ''),
(5, 0, 8, 'Jason', 'Lamotte', 'Sales', 'Nec ex augue labore, id possit omittam ullamcorper his. No vix ridens scaevola, ut laboramus argumentum vim. Ut singulis periculis scriptorem vix, volutpat ullamcorper sit te. Ex quod partem per. Eius deleniti at sit, regione phaedrum eu qui, at vocent pericula mea. Sale pertinacia ad vis, volumus omittam an mel, constituto mediocritatem sea ei.', ''),
(6, 0, 8, 'Sara', 'Jefferson', 'Sales', 'Idque doming percipitur eu mei. Everti consequat ei ius. Ut mel alia eirmod eripuit, ex vel omnis voluptatibus, omittam mentitum maluisset et eum. An quo delenit legimus menandri, pri ad brute ubique noluisse. Cu mei quot interpretaris. Mel cu prima soluta.', ''),
(7, 0, 8, 'Aretha', 'Hagopian', 'Sales', 'Pro dicta sonet definitionem te. Et munere malorum nam, pri id cibo illud, ius no vituperata ullamcorper. Lorem scribentur liberavisse quo ei, elit consetetur id usu. Suas adipiscing an mel, ea per graecis postulant, ad modus dicit elaboraret usu. Sed eirmod iriure patrioque ei. Et prompta accusata aliquando sed, ferri summo iudicabit ut his.', ''),
(8, 0, 8, 'Molly', 'Angelos', 'Sales', 'Idque doming percipitur eu mei. Everti consequat ei ius. Ut mel alia eirmod eripuit, ex vel omnis voluptatibus, omittam mentitum maluisset et eum. An quo delenit legimus menandri, pri ad brute ubique noluisse. Cu mei quot interpretaris. Mel cu prima soluta.', ''),
(9, 0, 1, 'Meta', 'Thorpe', 'Accounts', 'Ut per adhuc mentitum voluptatum, quas mollis inimicus ne cum. Nec at animal fabellas, at eum mutat hendrerit. Et vim petentium referrentur, tempor abhorreant sed no. Nulla quodsi nec ei. Et apeirian perfecto pri, ad nulla vituperata usu. Voluptua lucilius repudiandae ex nam, alia brute convenire nec eu.', ''),
(10, 0, 1, 'Vernetta', 'Pence', 'Accounts', 'Vel at vidit dicit, legere persecuti vix ne. Cum movet scaevola ut, nemore equidem democritum quo te. An idque ubique facilis vix. Te veri oblique pertinax has.', ''),
(11, 0, 1, 'Gerry', 'Darville', 'Accounts', 'Eu lorem postea eleifend usu. Elit inani dignissim cu pri. Te viderer eruditi forensibus eam, mel vidit mandamus te. Quis omnes consequuntur his ei, habeo exerci altera te mei, probo aperiri habemus et quo. Eius tritani dissentias ea pri, ex mundi instructior sed, eum eruditi meliore te. Iudicabit accommodare ea mei, te harum suscipit platonem sea.', ''),
(12, 0, 4, 'Jack', 'Joyce', 'IT Support', 'Nec ex augue labore, id possit omittam ullamcorper his. No vix ridens scaevola, ut laboramus argumentum vim. Ut singulis periculis scriptorem vix, volutpat ullamcorper sit te. Ex quod partem per. Eius deleniti at sit, regione phaedrum eu qui, at vocent pericula mea. Sale pertinacia ad vis, volumus omittam an mel, constituto mediocritatem sea ei.', ''),
(13, 0, 4, 'Hermila', 'Buntin', 'IT Support', 'Illum consetetur per ea. Vel an amet lorem, ex eos aperiri dissentiet liberavisse. Viris postea eu pri, nam falli officiis no. Cu duis definiebas ullamcorper mea. Ad duo feugait consulatu, et vim iudico lobortis. Ne civibus constituam scripserit has, ea affert doctus est. Harum everti facilisi vim id.', ''),
(14, 0, 4, 'Charlsie', 'Filice', 'IT Support', 'Fabellas euripidis pro no. Nam legere eloquentiam ei, mei quod fabulas pertinacia at. Duo no latine vidisse pericula, illud animal accusam mea no. Mazim sadipscing ad vis, pro et graeci intellegam. Id vim utinam docendi legendos, has in soleat suavitate. Mei ne unum omnium aliquam.', ''),
(15, 0, 4, 'Vivienne', 'Chambless', 'IT Support', 'No summo evertitur has. Ut simul scripserit usu. Cum id atomorum suscipiantur, vis eu tation aliquam officiis, nam ne solum singulis interpretaris. Has ne graeci dictas partiendo, has tation putant sanctus et, per ne melius qualisque. Mel vitae mandamus no, est an omnium accusam. In ferri atqui recusabo cum.', ''),
(16, 0, 4, 'Mauro', 'Noguera', 'IT Support', 'Nec ex augue labore, id possit omittam ullamcorper his. No vix ridens scaevola, ut laboramus argumentum vim. Ut singulis periculis scriptorem vix, volutpat ullamcorper sit te. Ex quod partem per. Eius deleniti at sit, regione phaedrum eu qui, at vocent pericula mea. Sale pertinacia ad vis, volumus omittam an mel, constituto mediocritatem sea ei.', ''),
(17, 0, 4, 'Hyman', 'Gagne', 'IT Support', 'Idque doming percipitur eu mei. Everti consequat ei ius. Ut mel alia eirmod eripuit, ex vel omnis voluptatibus, omittam mentitum maluisset et eum. An quo delenit legimus menandri, pri ad brute ubique noluisse. Cu mei quot interpretaris. Mel cu prima soluta.', ''),
(18, 0, 6, 'Melodee', 'Siemens', 'Marketing', 'Vel at vidit dicit, legere persecuti vix ne. Cum movet scaevola ut, nemore equidem democritum quo te. An idque ubique facilis vix. Te veri oblique pertinax has.', ''),
(19, 0, 6, 'Santa', 'Partridge', 'Marketing', 'Vix ad melius liberavisse disputationi. Ea his quod putent, vel ea verear adolescens. Has quod docendi deterruisset ne, sumo luptatum an cum, pri et accusata interesset. Aperiri fierent instructior ad his, mei eu ceteros mnesarchum. At mucius complectitur pro. Tamquam recteque voluptaria ut vel, ad his dolore audiam.', ''),
(20, 0, 6, 'Randal', 'Duffie', 'Marketing', 'Vel at vidit dicit, legere persecuti vix ne. Cum movet scaevola ut, nemore equidem democritum quo te. An idque ubique facilis vix. Te veri oblique pertinax has.', ''),
(21, 0, 6, 'Alison', 'Veazey', 'Marketing', 'Admodum ponderum sed ea, persecuti cotidieque ea vel. In sea placerat petentium aliquando, ullum appetere placerat an eos. Eos ad dicant oblique erroribus. Cu duis debet mea, idque mundi instructior in sea, te iriure dissentiunt sit.', ''),
(22, 0, 6, 'Elva', 'Korn', 'Marketing', 'Vix ad melius liberavisse disputationi. Ea his quod putent, vel ea verear adolescens. Has quod docendi deterruisset ne, sumo luptatum an cum, pri et accusata interesset. Aperiri fierent instructior ad his, mei eu ceteros mnesarchum. At mucius complectitur pro. Tamquam recteque voluptaria ut vel, ad his dolore audiam.', ''),
(23, 0, 7, 'Faviola', 'Horrigan', 'Research', 'Vel at vidit dicit, legere persecuti vix ne. Cum movet scaevola ut, nemore equidem democritum quo te. An idque ubique facilis vix. Te veri oblique pertinax has.', ''),
(24, 0, 7, 'Tynisha', 'Weinstock', 'Research', 'Nisl choro utinam per ne, munere ridens oblique no pri. Inani atomorum reformidans mel ne, nec ne equidem philosophia. Eius movet audiam eos cu, vel te quot duis tritani. Id alterum copiosae vix. Mea in diceret minimum tacimates. Sea eu inani commune tractatos.', ''),
(25, 0, 7, 'Jessenia', 'Gribble', 'Research', 'Fabellas euripidis pro no. Nam legere eloquentiam ei, mei quod fabulas pertinacia at. Duo no latine vidisse pericula, illud animal accusam mea no. Mazim sadipscing ad vis, pro et graeci intellegam. Id vim utinam docendi legendos, has in soleat suavitate. Mei ne unum omnium aliquam.', ''),
(26, 0, 7, 'Benton', 'Light', 'Research', 'Has cu duis tibique probatus, his et aperiri phaedrum scriptorem, summo platonem ut mea. Eam ne nonumes eligendi. Libris minimum in pro, usu modo labitur diceret ad. No sed option ancillae, et periculis tincidunt ius.', ''),
(27, 0, 7, 'Piper', 'Mundo', 'Research', 'Mea tota inani et, ad nam perfecto consulatu, pri semper fabulas officiis ut. Audiam admodum nominati eum eu, eam omnis debet suavitate ut, usu docendi recteque posidonium et. Te ius quaestio appellantur, nec in vide dicam scripta, has ex reque accumsan voluptatibus. Atqui explicari usu id. Ad nec quando dictas deleniti, nam amet utamur tincidunt ex, tale error nec id. Modus quidam veritus cu duo.', ''),
(28, 0, 2, 'Jenifer', 'Groth', 'Engineering', 'Idque doming percipitur eu mei. Everti consequat ei ius. Ut mel alia eirmod eripuit, ex vel omnis voluptatibus, omittam mentitum maluisset et eum. An quo delenit legimus menandri, pri ad brute ubique noluisse. Cu mei quot interpretaris. Mel cu prima soluta.', ''),
(29, 0, 2, 'Dominga', 'Maharaj', 'Engineering', 'Nec an soleat definitionem, usu lobortis conceptam et. Qualisque expetendis pri ex. Te mea solet epicuri convenire, etiam falli dicam eum no. Falli torquatos id has, duo id timeam persequeris. Pri ne laudem dissentias. Essent mollis pri et, ius elitr primis efficiantur ea.', ''),
(30, 0, 2, 'Rudolf', 'Alexander', 'Engineering', 'Idque doming percipitur eu mei. Everti consequat ei ius. Ut mel alia eirmod eripuit, ex vel omnis voluptatibus, omittam mentitum maluisset et eum. An quo delenit legimus menandri, pri ad brute ubique noluisse. Cu mei quot interpretaris. Mel cu prima soluta.', ''),
(31, 0, 2, 'Ilene', 'Tyner', 'Engineering', 'Atqui euripidis eos ad, in est meliore nominavi indoctum. Ignota quaeque fierent quo ei, ad ius audire habemus. Odio facer te mei, nec no eruditi petentium disputando. Et his probo quaeque splendide. Duo ad semper scripta commune, justo populo no mea, ei vero falli quo.', ''),
(32, 0, 2, 'Marvel', 'Overby', 'Engineering', 'Vix ad melius liberavisse disputationi. Ea his quod putent, vel ea verear adolescens. Has quod docendi deterruisset ne, sumo luptatum an cum, pri et accusata interesset. Aperiri fierent instructior ad his, mei eu ceteros mnesarchum. At mucius complectitur pro. Tamquam recteque voluptaria ut vel, ad his dolore audiam.', ''),
(33, 0, 2, 'Shenita', 'Pouncey', 'Engineering', 'Nisl choro utinam per ne, munere ridens oblique no pri. Inani atomorum reformidans mel ne, nec ne equidem philosophia. Eius movet audiam eos cu, vel te quot duis tritani. Id alterum copiosae vix. Mea in diceret minimum tacimates. Sea eu inani commune tractatos.', ''),
(34, 0, 2, 'Mikki', 'Laing', 'Engineering', 'Has cu duis tibique probatus, his et aperiri phaedrum scriptorem, summo platonem ut mea. Eam ne nonumes eligendi. Libris minimum in pro, usu modo labitur diceret ad. No sed option ancillae, et periculis tincidunt ius.', ''),
(35, 0, 2, 'Josefa', 'Purcell', 'Engineering', 'Has cu duis tibique probatus, his et aperiri phaedrum scriptorem, summo platonem ut mea. Eam ne nonumes eligendi. Libris minimum in pro, usu modo labitur diceret ad. No sed option ancillae, et periculis tincidunt ius.', ''),
(36, 0, 2, 'Gayle', 'Cash', 'Engineering', 'Fabellas euripidis pro no. Nam legere eloquentiam ei, mei quod fabulas pertinacia at. Duo no latine vidisse pericula, illud animal accusam mea no. Mazim sadipscing ad vis, pro et graeci intellegam. Id vim utinam docendi legendos, has in soleat suavitate. Mei ne unum omnium aliquam.', ''),
(37, 0, 2, 'Mitzie', 'Cleek', 'Engineering', 'Prompta labores no his. Vix tritani scaevola disputando et, vix dicat vulputate sadipscing ea. His mundi invidunt an, summo lucilius suavitate vis ut. Cu vidit munere repudiandae sea. Pro ad iusto graecis, ei vis epicuri intellegat.', ''),
(38, 0, 2, 'Walton', 'Andress', 'Engineering', 'Nisl choro utinam per ne, munere ridens oblique no pri. Inani atomorum reformidans mel ne, nec ne equidem philosophia. Eius movet audiam eos cu, vel te quot duis tritani. Id alterum copiosae vix. Mea in diceret minimum tacimates. Sea eu inani commune tractatos.', ''),
(39, 0, 2, 'Cinthia', 'Rusher', 'Engineering', 'Duo diam movet te, facer democritum ex ius. Dignissim consectetuer eam ne. No audire accusata lobortis duo, ei stet aeterno has, adhuc labores te sed. Mei in invenire efficiantur, prima malorum in eos, tale vero no pro. Suas corrumpit te pri, te cum tation mollis laoreet, at agam probatus sit. Case iusto euismod nam ex, vero nulla errem eu sea.', ''),
(40, 0, 2, 'Lois', 'Garner', 'Engineering', 'Nisl choro utinam per ne, munere ridens oblique no pri. Inani atomorum reformidans mel ne, nec ne equidem philosophia. Eius movet audiam eos cu, vel te quot duis tritani. Id alterum copiosae vix. Mea in diceret minimum tacimates. Sea eu inani commune tractatos.', ''),
(41, 0, 2, 'Clay', 'Igoe', 'Engineering', 'Nec ex augue labore, id possit omittam ullamcorper his. No vix ridens scaevola, ut laboramus argumentum vim. Ut singulis periculis scriptorem vix, volutpat ullamcorper sit te. Ex quod partem per. Eius deleniti at sit, regione phaedrum eu qui, at vocent pericula mea. Sale pertinacia ad vis, volumus omittam an mel, constituto mediocritatem sea ei.', ''),
(42, 0, 5, 'Dania', 'Corona', 'Legal', 'Nisl choro utinam per ne, munere ridens oblique no pri. Inani atomorum reformidans mel ne, nec ne equidem philosophia. Eius movet audiam eos cu, vel te quot duis tritani. Id alterum copiosae vix. Mea in diceret minimum tacimates. Sea eu inani commune tractatos.', ''),
(43, 0, 5, 'Magali', 'Six', 'Legal', 'Atqui euripidis eos ad, in est meliore nominavi indoctum. Ignota quaeque fierent quo ei, ad ius audire habemus. Odio facer te mei, nec no eruditi petentium disputando. Et his probo quaeque splendide. Duo ad semper scripta commune, justo populo no mea, ei vero falli quo.', ''),
(44, 0, 5, 'Milly', 'Caul', 'Legal', 'Nisl choro utinam per ne, munere ridens oblique no pri. Inani atomorum reformidans mel ne, nec ne equidem philosophia. Eius movet audiam eos cu, vel te quot duis tritani. Id alterum copiosae vix. Mea in diceret minimum tacimates. Sea eu inani commune tractatos.', ''),
(45, 0, 5, 'Shirl', 'Ludwig', 'Legal', 'Nec ex augue labore, id possit omittam ullamcorper his. No vix ridens scaevola, ut laboramus argumentum vim. Ut singulis periculis scriptorem vix, volutpat ullamcorper sit te. Ex quod partem per. Eius deleniti at sit, regione phaedrum eu qui, at vocent pericula mea. Sale pertinacia ad vis, volumus omittam an mel, constituto mediocritatem sea ei.', ''),
(46, 0, 5, 'Maude', 'Mcconville', 'Legal', 'Atqui euripidis eos ad, in est meliore nominavi indoctum. Ignota quaeque fierent quo ei, ad ius audire habemus. Odio facer te mei, nec no eruditi petentium disputando. Et his probo quaeque splendide. Duo ad semper scripta commune, justo populo no mea, ei vero falli quo.', ''),
(47, 0, 3, 'Jesenia', 'Cardello', 'Human Resources', 'Idque doming percipitur eu mei. Everti consequat ei ius. Ut mel alia eirmod eripuit, ex vel omnis voluptatibus, omittam mentitum maluisset et eum. An quo delenit legimus menandri, pri ad brute ubique noluisse. Cu mei quot interpretaris. Mel cu prima soluta.', ''),
(48, 0, 3, 'Myesha', 'Mulcahy', 'Human Resources', 'Nec ex augue labore, id possit omittam ullamcorper his. No vix ridens scaevola, ut laboramus argumentum vim. Ut singulis periculis scriptorem vix, volutpat ullamcorper sit te. Ex quod partem per. Eius deleniti at sit, regione phaedrum eu qui, at vocent pericula mea. Sale pertinacia ad vis, volumus omittam an mel, constituto mediocritatem sea ei.', ''),
(49, 0, 3, 'Tanner', 'Alling', 'Human Resources', 'Atqui euripidis eos ad, in est meliore nominavi indoctum. Ignota quaeque fierent quo ei, ad ius audire habemus. Odio facer te mei, nec no eruditi petentium disputando. Et his probo quaeque splendide. Duo ad semper scripta commune, justo populo no mea, ei vero falli quo.', ''),
(50, 0, 3, 'Kandice', 'Frenz', 'Human Resources', 'Nisl choro utinam per ne, munere ridens oblique no pri. Inani atomorum reformidans mel ne, nec ne equidem philosophia. Eius movet audiam eos cu, vel te quot duis tritani. Id alterum copiosae vix. Mea in diceret minimum tacimates. Sea eu inani commune tractatos.', '');
COMMIT;

