<?php

namespace App\Http\Controllers\Web;

use App\Models\NetvalueStaff;
use App\Models\NetvalueDepartment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NetvalueStaffController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //return $request;
        $data['departments_ary'] = NetvalueDepartment::orderBy('name','asc')->pluck('name', 'id')->toArray();
        $data['departments_ary'][0] = 'All Departments';

        $data['all_staff'] = NetvalueStaff::with('department')
        ->when( (trim(request()->key) !=''), function ($query) {
            return $query->where(function ($query2) {
                return $query2->where('profile',  'like', '%'.trim(request()->key).'%')
                                ->orWhere('firstname',  'like', '%'.trim(request()->key).'%')
                                ->orWhere('lastname',  'like', '%'.trim(request()->key).'%');
            });
        }) 
        ->when( ( (int)request()->department_id >0 ), function ($query) {
            return $query->where('department_id', trim(request()->department_id) );
        })         
        ->orderBy('firstname','asc')
        ->paginate(10);

        
        //dd($data);
        return view('netvalue.staff_index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(NetvalueStaff $category)
    {

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NetvalueStaff $category)
    {

        
    }

    public function clone(Request $request, NetvalueStaff $category)
    {

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, NetvalueStaff $category)
    {

        
    }
}
