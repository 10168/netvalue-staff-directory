<?php

namespace App\Http\Controllers\Api;

use App\Models\NetvalueStaff;
use App\Models\NetvalueDepartment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiNetvalueStaffController extends Controller
{

    /**
     * Get staff json data
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit =  ( !is_numeric(request()->limit) || request()->limit > 30 ) ? 10 : request()->limit;
        $offset =  ( !is_numeric(request()->offset) ) ? 0 : request()->offset;

        $all_staff = NetvalueStaff::with('department')
        ->when( ( (int)request()->department_id >0 ), function ($query) {
            return $query->where('department_id', (int)request()->department_id );
        })          
        ->when( (trim(request()->key) !=''), function ($query) {
            return $query->where(function ($query2) {
                return $query2->where('profile',  'like', '%'.trim(request()->key).'%')
                                ->orWhere('firstname',  'like', '%'.trim(request()->key).'%')
                                ->orWhere('lastname',  'like', '%'.trim(request()->key).'%');
            });
        }) 
       
        ->orderBy('firstname','asc')
        ->offset($offset)->limit($limit)->get(); 

        return $all_staff;
    }

}
