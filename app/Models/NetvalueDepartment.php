<?php

/**
 * Created by Alex Zeng
 * Date: Thu, 28 Jun 2018 14:13:56 +1200.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NetvalueDepartment
 * 
 * @property int $id
 * @property string $name
 * 
 * @property \Illuminate\Database\Eloquent\Collection $netvalue_staffs
 *
 * @package App\Models
 */
class NetvalueDepartment extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name'
	];

	public function netvalue_staffs()
	{
		return $this->hasMany(\App\Models\NetvalueStaff::class, 'department_id');
	}
}
