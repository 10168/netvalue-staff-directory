<?php

/**
 * Created by Alex Zeng
 * Date: Thu, 28 Jun 2018 14:12:22 +1200.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NetvalueStaff
 * 
 * @property int $id
 * @property int $user_id
 * @property int $department_id
 * @property string $firstname
 * @property string $lastname
 * @property string $department
 * @property string $profile
 * @property string $avatar
 * 
 * @property \App\Models\NetvalueDepartment $netvalue_department
 *
 * @package App\Models
 */
class NetvalueStaff extends Eloquent
{
	protected $table = 'netvalue_staff';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'department_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'department_id',
		'firstname',
		'lastname',
		'department_name',
		'profile',
		'avatar'
	];

	public function department()
	{
		return $this->belongsTo(\App\Models\NetvalueDepartment::class, 'department_id');
	}
}
