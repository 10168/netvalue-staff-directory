// This class display search form and get the data from RESTful API
// modified on 29/06/2018 by Alex Zeng
class LoadStaffData extends React.Component<any, any> {
    constructor(props) {
        super(props);
    
        this.state = {
          offset: 0,
          limit:10,
          total:0,
          no_more_data: false,
          loading: false,
          button_text: 'Show More Data',
          department_id: 0,
          key: '',
          all_staff: []       
        };

        // bind
        this.LoadMoreData = this.LoadMoreData.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
    // set state by input name
    handleChange(event){
        var newState = this.state;
        newState[event.target.name] = event.target.value;

        console.log(newState);
        this.setState({newState});

        this.setState({ offset: 0, total:0 });
    }

    // load some data and display it when page initialized
    componentDidMount() {
        this.LoadMoreData(0);
    }

    /* This method is invoked when submit button is pressed */
    handleSubmit(e) {
        //preventDefault prevents page reload   
        e.preventDefault();
        this.setState({ offset: 0, total:0, no_more_data: false, all_staff: [] });
        console.log(this.state);
        this.LoadMoreData(0);

        window.need_highlight = true;
    }
  
    // click load more button
    LoadMoreData(offset_num = -1)    {
        this.setState({ loading: true, button_text: 'Loading Data......' });
        if ( offset_num == -1 || typeof(offset_num) != 'number'){
            offset_num = this.state.offset;
        }

        axios.get(`/api/v1/NetValueStaff`, {
            params: {
                key: this.state.key,
                department_id: this.state.department_id,
                limit: this.state.limit,
                offset: offset_num
            }
          })
          .then(res => {
            let new_data = res.data;
            if(new_data.length < this.state.limit) {
                this.setState({ no_more_data: true });
            }
            this.setState({ 
                all_staff: [...this.state.all_staff, ...new_data],
                offset: this.state.offset + this.state.limit,
                total: this.state.total + new_data.length,
                loading: false, button_text: 'Show More Data' 
            });
          }).catch((error) => {
            // Error
            this.setState({ loading: true, button_text: 'Error happened, please let us know' });
        });
          
    }



    render() {

        return (<div><a name="search"></a>
        <div className="row pr-1 pl-1">
            <form onSubmit={this.handleSubmit} className=" w-100">
            <div className="input-group">

                {/* keyword input */}
                <input className="form-control" style={{'border-right':0}} id="search_keyword" placeholder="Keywords"  name="key" type="text"
                value={this.state.key}
                onChange={this.handleChange} 
                />

                {/* department input */}
                <select className=" input-group-append my_select" style={{'border-color':'#ced4da', 'border-radius':0, 'border-right':0, 'border-left':0,'background-color':'white','border-top':'1px solid #ced4da', 'border-bottom':'1px solid #ced4da'}} id="department_id" name="department_id" onChange={this.handleChange} >
                    <option value="0" selected="selected">All Departments</option>
                    <option value="1">Accounts</option>
                    <option value="2">Engineering</option>
                    <option value="3">Human Resources</option>
                    <option value="4">IT Support</option>
                    <option value="5">Legal</option>
                    <option value="6">Marketing</option>
                    <option value="7">Research</option>
                    <option value="8">Sales</option>
                </select>
                

                {/* search icon */}
                <span className="input-group-addon"><a href="#" onClick={this.handleSubmit} className="text-primary form-control" style={{'border-top-left-radius':0, 'border-bottom-left-radius':0, 'border-left':0}}><i class="fa fa-search mr-1"></i></a></span>

            </div>
            </form>

        </div>

        <div className="row pr-1 pl-1">
          <ul className="list-group mt-2  w-100" id="all_staff">
            {/* no result */}
            {( this.state.all_staff.length == 0 && this.state.loading == false ) ? <div class="text-danger mt-5 mb-1 text-center w-100 large"><i className="fa fa-coffee shadow text-danger" aria-hidden="true"></i> Whoops!<br/>Seems there is no match. Please try another keyword</div> : '' }

            {/* display results */}
            {this.state.all_staff.map(staff =>
                <li key={staff.id} className="list-group-item  list-group-item-action">
                    <img className="rounded float-left mr-1 avatar_img" src="/images/default_profile.png" alt="avatar" />

                    <div className="highlight-text font-weight-bold">{staff.firstname}  {staff.lastname}</div>
                    <div className="text-info">{staff.department.name}</div>
                    <div className="highlight-text small">{staff.profile}</div>

                </li>
            )}
            
            {/* load more button */}
            {this.state.no_more_data == true ? <div className="text-center w-100 mb-2 mt-3 small text-success">{ this.state.total>1  ?  `There are total ${this.state.total} results` : ( this.state.total==1 ? 'There is only 1 result' : '') }</div> : <div className="text-center w-100"><div className="text-center mb-2 mt-3"><button type="button" className="btn btn-success" onClick={this.LoadMoreData}>{this.state.button_text}</button></div></div>}

          </ul>
        </div>

        {/* extra links */}

        { (this.state.key != '' || this.state.department_id > 0 )  ? <div className="text-center mt-4 small"><a class="text-info" href="./Staff-ReactJS"><i class="fa fa-user-circle mr-1"></i>Show All Staff</a></div> : ''}

        <div className="text-center mt-3 small"><a href="#top"><i class="fa fa-search-plus mr-1"></i>Try Another Search</a></div>

        </div>
        );  
    }
}


ReactDOM.render(<LoadStaffData Infomation=""  />,  
document.getElementById('staff_search_result') 
);

