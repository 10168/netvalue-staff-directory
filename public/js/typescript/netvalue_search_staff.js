var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// This class display search form and get the data from RESTful API
// modified on 29/06/2018 by Alex Zeng
var LoadStaffData = /** @class */ (function (_super) {
    __extends(LoadStaffData, _super);
    function LoadStaffData(props) {
        var _this = _super.call(this, props) || this;
        _this.state = {
            offset: 0,
            limit: 10,
            total: 0,
            no_more_data: false,
            loading: false,
            button_text: 'Show More Data',
            department_id: 0,
            key: '',
            all_staff: []
        };
        // bind
        _this.LoadMoreData = _this.LoadMoreData.bind(_this);
        _this.handleChange = _this.handleChange.bind(_this);
        _this.handleSubmit = _this.handleSubmit.bind(_this);
        return _this;
    }
    // set state by input name
    LoadStaffData.prototype.handleChange = function (event) {
        var newState = this.state;
        newState[event.target.name] = event.target.value;
        console.log(newState);
        this.setState({ newState: newState });
        this.setState({ offset: 0, total: 0 });
    };
    // load some data and display it when page initialized
    LoadStaffData.prototype.componentDidMount = function () {
        this.LoadMoreData(0);
    };
    /* This method is invoked when submit button is pressed */
    LoadStaffData.prototype.handleSubmit = function (e) {
        //preventDefault prevents page reload   
        e.preventDefault();
        this.setState({ offset: 0, total: 0, no_more_data: false, all_staff: [] });
        console.log(this.state);
        this.LoadMoreData(0);
        window.need_highlight = true;
    };
    // click load more button
    LoadStaffData.prototype.LoadMoreData = function (offset_num) {
        var _this = this;
        if (offset_num === void 0) { offset_num = -1; }
        this.setState({ loading: true, button_text: 'Loading Data......' });
        if (offset_num == -1 || typeof (offset_num) != 'number') {
            offset_num = this.state.offset;
        }
        axios.get("/api/v1/NetValueStaff", {
            params: {
                key: this.state.key,
                department_id: this.state.department_id,
                limit: this.state.limit,
                offset: offset_num
            }
        })
            .then(function (res) {
            var new_data = res.data;
            if (new_data.length < _this.state.limit) {
                _this.setState({ no_more_data: true });
            }
            _this.setState({
                all_staff: _this.state.all_staff.concat(new_data),
                offset: _this.state.offset + _this.state.limit,
                total: _this.state.total + new_data.length,
                loading: false, button_text: 'Show More Data'
            });
        }).catch(function (error) {
            // Error
            _this.setState({ loading: true, button_text: 'Error happened, please let us know' });
        });
    };
    LoadStaffData.prototype.render = function () {
        return (React.createElement("div", null,
            React.createElement("a", { name: "search" }),
            React.createElement("div", { className: "row pr-1 pl-1" },
                React.createElement("form", { onSubmit: this.handleSubmit, className: " w-100" },
                    React.createElement("div", { className: "input-group" },
                        React.createElement("input", { className: "form-control", style: { 'border-right': 0 }, id: "search_keyword", placeholder: "Keywords", name: "key", type: "text", value: this.state.key, onChange: this.handleChange }),
                        React.createElement("select", { className: " input-group-append my_select", style: { 'border-color': '#ced4da', 'border-radius': 0, 'border-right': 0, 'border-left': 0, 'background-color': 'white', 'border-top': '1px solid #ced4da', 'border-bottom': '1px solid #ced4da' }, id: "department_id", name: "department_id", onChange: this.handleChange },
                            React.createElement("option", { value: "0", selected: "selected" }, "All Departments"),
                            React.createElement("option", { value: "1" }, "Accounts"),
                            React.createElement("option", { value: "2" }, "Engineering"),
                            React.createElement("option", { value: "3" }, "Human Resources"),
                            React.createElement("option", { value: "4" }, "IT Support"),
                            React.createElement("option", { value: "5" }, "Legal"),
                            React.createElement("option", { value: "6" }, "Marketing"),
                            React.createElement("option", { value: "7" }, "Research"),
                            React.createElement("option", { value: "8" }, "Sales")),
                        React.createElement("span", { className: "input-group-addon" },
                            React.createElement("a", { href: "#", onClick: this.handleSubmit, className: "text-primary form-control", style: { 'border-top-left-radius': 0, 'border-bottom-left-radius': 0, 'border-left': 0 } },
                                React.createElement("i", { class: "fa fa-search mr-1" })))))),
            React.createElement("div", { className: "row pr-1 pl-1" },
                React.createElement("ul", { className: "list-group mt-2  w-100", id: "all_staff" },
                    (this.state.all_staff.length == 0 && this.state.loading == false) ? React.createElement("div", { class: "text-danger mt-5 mb-1 text-center w-100 large" },
                        React.createElement("i", { className: "fa fa-coffee shadow text-danger", "aria-hidden": "true" }),
                        " Whoops!",
                        React.createElement("br", null),
                        "Seems there is no match. Please try another keyword") : '',
                    this.state.all_staff.map(function (staff) {
                        return React.createElement("li", { key: staff.id, className: "list-group-item  list-group-item-action" },
                            React.createElement("img", { className: "rounded float-left mr-1 avatar_img", src: "/images/default_profile.png", alt: "avatar" }),
                            React.createElement("div", { className: "highlight-text font-weight-bold" },
                                staff.firstname,
                                "  ",
                                staff.lastname),
                            React.createElement("div", { className: "text-info" }, staff.department.name),
                            React.createElement("div", { className: "highlight-text small" }, staff.profile));
                    }),
                    this.state.no_more_data == true ? React.createElement("div", { className: "text-center w-100 mb-2 mt-3 small text-success" }, this.state.total > 1 ? "There are total " + this.state.total + " results" : (this.state.total == 1 ? 'There is only 1 result' : '')) : React.createElement("div", { className: "text-center w-100" },
                        React.createElement("div", { className: "text-center mb-2 mt-3" },
                            React.createElement("button", { type: "button", className: "btn btn-success", onClick: this.LoadMoreData }, this.state.button_text))))),
            (this.state.key != '' || this.state.department_id > 0) ? React.createElement("div", { className: "text-center mt-4 small" },
                React.createElement("a", { class: "text-info", href: "./Staff-ReactJS" },
                    React.createElement("i", { class: "fa fa-user-circle mr-1" }),
                    "Show All Staff")) : '',
            React.createElement("div", { className: "text-center mt-3 small" },
                React.createElement("a", { href: "#top" },
                    React.createElement("i", { class: "fa fa-search-plus mr-1" }),
                    "Try Another Search"))));
    };
    return LoadStaffData;
}(React.Component));
ReactDOM.render(React.createElement(LoadStaffData, { Infomation: "" }), document.getElementById('staff_search_result'));
