var old_keyword = '';
var staff_num = 0;
var highlight_options = {
    "element": "span",
    "className": "text-danger font-weight-bold"
}

// highlight the keyword of the result
function highlightNow() {
    if ( $('#search_keyword').val() != '' && $('#all_staff li').length > 0 && ( $('#search_keyword').val() != old_keyword || staff_num != $('#all_staff li').length )  )  {

        $('.highlight-text').unmark(highlight_options);
        $(".highlight-text").mark($('#search_keyword').val(), highlight_options);

    }

    old_keyword = $('#search_keyword').val();
    staff_num = $('#all_staff li').length;
    
    setTimeout(highlightNow, 500);
}

highlightNow();