@extends('netvalue.layout')

@section('content')

<div id="staff_search_result"></div>


{{-- react js --}}
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<script crossorigin src="https://unpkg.com/axios/dist/axios.min.js"></script>
{{-- support IE browser --}}
@if ( strpos($_SERVER['HTTP_USER_AGENT'], "Trident") != false || strpos($_SERVER['HTTP_USER_AGENT'], "MSIE") != false )
    <script crossorigin src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.5.7/core.min.js"></script>
@endif
<script src="/js/typescript/netvalue_search_staff.js"></script>


{{-- highlight keyword --}}
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.min.js'></script>
<script src="/js/netvalue_highlight.js"></script>


@endsection
