@extends('netvalue.layout')

@section('content')

{{-- search form --}}
<div class="row mb-3 pr-1 pl-1">
    <div class="card w-100">
        <div class="card-header bg-success text-light "><i class="fa fa-search-plus mr-1"></i>Staff Search</div>
        <div class="card-body pt-1">

            {!! Form::model('Staff', ['route' => ['Staff.index'], 'method' => "GET", 'files'=>true,'class'=>'w-100','onsubmit'=>'return check_search_form(this);', 'name'=>'search_form']) !!}
            <div class="row">
                <div class="col-md form-group pl-0 pr-1">

                    {!! Form::text('key', ( isset(request()->key) ? trim(request()->key) : ''), ['class'=>'form-control',  'id'=>'search_keyword','placeholder'=>'Keyword, eg. name, skills   ']); !!}

                </div> 


                <div class="col-md-auto form-group pl-0 pr-1">
                    {!! Form::select('department_id', $departments_ary, ( isset(request()->department_id) ? request()->department_id : 0) , ['class'=>'form-control my_select',  'id'=>'department_id']); !!}
                </div>

                <div class="col-md-auto form-group p-0">
                    {!! Form::submit('Search', ['class'=>'btn btn-primary text-center', 'id'=>'submit_btn']); !!}
                </div>

               

            </div> 
            {!! Form::close() !!}
        </div>
    </div>
</div>

@forelse ($all_staff as $staff)
    @if ( $loop->first)
    {{-- show search hint --}}
    <div class="row pr-1 pl-1">
        <div class="col text-info small pl-0 pr-0">
            @if ( trim(request()->key) != '')
                Searching <span class="d-none d-md-inline-block">keyword</span> <span class='font-weight-bold'>{{ request()->key }}</span> in {{$departments_ary[(int)request()->department_id]}} 
                {{ ((int)request()->department_id > 0 ) ? ' Department ' : ' ' }}
            @else
                Showing all staff in {{$departments_ary[(int)request()->department_id]}} 
                {{ ((int)request()->department_id > 0 ) ? ' Department ' : ' ' }}
            @endif

            @if ( (int)request()->department_id > 0 || trim(request()->key) != '' )
                <div class="float-right"><a href="./Staff"><i class="fa fa-user-circle mr-1"></i>Reset</a></div>
            @endif
        </div>
    </div>
    
    @endif
    {{-- search results --}}
    <div class="row mb-2 pr-1 pl-1">
        <div class="card w-100">

            <div class="card-body p-1">
                    <img class="rounded float-left mr-1 avatar_img" src="/images/default_profile.png" alt="avatar" >

                <div class="highlight-text font-weight-bold">{{ $staff->firstname . ' ' . $staff->lastname}}</div>
                <div class="text-info"><a href="?department_id={{ $staff->department->id}}" class="text-info">{{ $staff->department->name}}</a></div>
                <div class="highlight-text small">{{ $staff->profile }}</div>
            </div>
        </div>
    </div>

    @if ( $loop->last)
    {{-- pagination --}}
        <div class="row text-right pr-1 pl-1">
            <div class="col text-info small">
                @if ( $all_staff->total() > 1 )
                    Total results {{$all_staff->total()}}
                @endif
                @if ( $all_staff->lastPage() > 1 )
                    , current page is {{$all_staff->currentPage()}} of {{$all_staff->lastPage()}}
                @endif
            </div>
        </div>

        <div class="w-100 text-center">
            <div class=" d-inline-block mt-2">{{ $all_staff->appends(['key' => request()->key, 'department_id' => request()->department_id])->links() }}</div>
        </div> 
    @endif
@empty
{{-- no result --}}
    <div class="text-danger mt-5 mb-5 text-center w-100 large"><i class="fa fa-coffee shadow text-danger" aria-hidden="true"></i> Whoops!<br/>Seems there is no match. Please try another keyword</div>

@endforelse



{{-- search form verification --}}
<script>
function check_search_form(f){
    if( f.key.value.length>15 ){
        alert('Keyword should less than 15 characters');
        return false;
    }
    return true;
}
</script>


{{-- highlight search keyword, load last --}}
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.min.js'></script>
<script>
    var highlight_options = {
        "element": "span",
        "className": "text-danger font-weight-bold"
    }
    $(".highlight-text").mark('{{trim(request()->key)}}', highlight_options);
</script>


@endsection
