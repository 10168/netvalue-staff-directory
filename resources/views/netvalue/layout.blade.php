<!DOCTYPE html>
<html lang=en>
<head>
    <meta charset="utf-8">
    <title>Web Developer Test Page</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0, user-scalable=no">
    <meta name="author" content="Alex Zeng">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/netvalue_staff.css">
</head>
<body>

{{-- top part --}}
<div class="container page_top">
  <div class="row justify-content-between" id="top_menu">
    <div class="col text-center text-weight-bold">
        <a name="top"></a>
        <h1>Staff Directory</h1>
    </div>

  </div>
</div>


{{-- main part --}}
<div class="container page_content">
@yield('content')
</div>


{{-- bottom part --}}

<div class="container page_bottom mt-5">

<div class="row" id="bottom_text">

    <div class="col text-center mb-3 small">

      @if ( Request::route()->getName() != 'Staff-ReactJS')
          <div><a href="./Staff-ReactJS" class="small text-success"><i class="fa fa-arrow-circle-right mr-1" ></i>Switch to ReactJS+RESTful Api version</a></div>
      @else
          <div><a href="./Staff" class="small text-success"><i class="fa fa-arrow-circle-right mr-1" ></i> Switch to Laravel+Bootstrap version</a></div>
      @endif

      <div class="mt-3">www.StaffDirectory.co.nz <i class="fa fa-home shadow" style="color:#005c9a;"></i> 2018-2020</div>
        
    </div>
  </div>

</div>




</body>
</html>