<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/','Web\NetvalueStaffController');
Route::resource('Staff','Web\NetvalueStaffController');
Route::view('Staff-ReactJS', 'netvalue.staff_reactjs')->name('Staff-ReactJS');

