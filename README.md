**NetValue Staff Directory Test Project**

This project create a searchable Staff Directory. First name, last name, details are all searchable fields. And it scale to all common devices. eg. ios or android smart phone, ipad or other tablet, desktop pc.


This project use PHP + MySql + Laravel Framework + Bootstrap CSS Framework + jQuery + ReactJS

It's two versions:

Version 1:

    Test path: /Staff

    Frameworks or Libraris: Laravel + Bootstrap + jQuery + Blade Template

    Demo URL: http://45.32.242.5:8082/Staff


Version 2: 

    Test path: /Staff-ReactJS

    Frameworks or Libraris: ReactJS + RESTful API + Laravel + Bootstrap

    Demo URL: http://45.32.242.5:8082/Staff-ReactJS

---

## Install PHP+MYSQL

Minimum requirement is PHP7.x + MySql5.x

---

## Install Laravel

Document: https://laravel.com/docs/5.6/installation

Main Steps:

1. composer global require "laravel/installer"
2. Choose a directory to setup this project, eg. cd F:\document_root_002 
3. laravel new netvalue.test
4. Setup a virtual host(hostname netvalue.test), config the document root path to  F:\document_root_002\netvalue.test\public
5. visit http://netvalue.test/ and you will see Laravel welcome page

Install Required Laravel Components:

cd netvalue.test

composer require "laravelcollective/html"

netvalue.test>composer require reliese/laravel


---

## Install test project/code 

Unzip the alex.netvalue.test.code.zip

Copy all folds(app, routes, config, public, reources) to netvalue.test/

Overwrite all the exists flods and files

Edit netvalue.test/.env file to modify the database connection variables. eg.

    DB_DATABASE=netvalue_test

    DB_USERNAME=root

    DB_PASSWORD=


Import mysql data from netvalue.test/database/netvalue_test.sql


Note: this sql file will try to create a database netvalue_test, modify it if it doesn't suit.

